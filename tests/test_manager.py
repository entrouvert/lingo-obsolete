from django.contrib.auth.models import User
from django.core.wsgi import get_wsgi_application
from webtest import TestApp
import pytest

from lingo.models import Regie

pytestmark = pytest.mark.django_db

@pytest.fixture
def admin_user():
    try:
        user = User.objects.get(username='admin')
    except User.DoesNotExist:
        user = User.objects.create_superuser('admin', email=None, password='admin')
    return user

def login(app, username='admin', password='admin'):
    login_page = app.get('/login/')
    login_form = login_page.forms[0]
    login_form['username'] = username
    login_form['password'] = password
    resp = login_form.submit()
    assert resp.status_int == 302
    return app

def test_access(admin_user):
    app = login(TestApp(get_wsgi_application()))
    resp = app.get('/manage/', status=200)
    assert '/manage/lingo/' in resp.body

def test_add_regie(admin_user):
    Regie.objects.all().delete()
    app = login(TestApp(get_wsgi_application()))
    resp = app.get('/manage/lingo/regies/', status=200)
    resp = resp.click('New')
    resp.forms[0]['label'] = 'Test'
    resp.forms[0]['slug'] = 'test'
    resp.forms[0]['description'] = 'description'
    resp.forms[0]['service'] = 'dummy'
    resp = resp.forms[0].submit()
    assert resp.location == 'http://localhost:80/manage/lingo/regies/'
    assert Regie.objects.count() == 1
    regie = Regie.objects.all()[0]
    assert regie.label == 'Test'

def test_edit_regie(admin_user):
    test_add_regie(admin_user)
    app = login(TestApp(get_wsgi_application()))
    resp = app.get('/manage/lingo/regies/', status=200)
    resp = resp.click('Test')
    resp.forms[0]['description'] = 'other description'
    resp = resp.forms[0].submit()
    assert resp.location == 'http://localhost:80/manage/lingo/regies/'
    assert Regie.objects.count() == 1
    regie = Regie.objects.all()[0]
    assert regie.description == 'other description'

def test_delete_regie(admin_user):
    test_add_regie(admin_user)
    app = login(TestApp(get_wsgi_application()))
    resp = app.get('/manage/lingo/regies/', status=200)
    resp = resp.click('Test')
    resp = resp.click('Delete')
    assert 'Are you sure you want to delete this?' in resp.body
    resp = resp.forms[0].submit()
    assert resp.location == 'http://localhost:80/manage/lingo/regies/'
    assert Regie.objects.count() == 0
