import pytest

from django.contrib.auth.models import User
from django.test.client import RequestFactory
from django.template import Context
from django.utils import timezone

from combo.data.models import Page
from lingo.models import Regie, BasketItem, Transaction
from lingo.models import LingoBasketCell, LingoRecentTransactionsCell

pytestmark = pytest.mark.django_db

@pytest.fixture
def user():
    try:
        user = User.objects.get(username='admin')
    except User.DoesNotExist:
        user = User.objects.create_user('admin', email=None, password='admin')
    return user

@pytest.fixture
def regie():
    try:
        regie = Regie.objects.get(slug='test')
    except Regie.DoesNotExist:
        regie = Regie()
        regie.label = 'Test'
        regie.slug = 'test'
        regie.description = 'test'
        regie.save()
    return regie

def test_cell_disabled():
    Regie.objects.all().delete()
    assert LingoBasketCell.is_enabled() is False
    assert LingoRecentTransactionsCell.is_enabled() is False

def test_cell_enabled(regie):
    assert LingoBasketCell.is_enabled() is True
    assert LingoRecentTransactionsCell.is_enabled() is True

def test_basket_cell(regie, user):
    page = Page(title='xxx', slug='test_basket_cell', template_name='standard')
    page.save()
    cell = LingoBasketCell(page=page, placeholder='content', order=0)

    context = Context({'request': RequestFactory().get('/')})
    context['request'].user = None
    assert cell.is_relevant(context) is False
    context['request'].user = user
    assert cell.is_relevant(context) is False
    item  = BasketItem()
    item.user = user
    item.regie = regie
    item.subject = 'foo'
    item.source_url = 'http://example.net'
    item.amount = 12345
    item.save()
    assert cell.is_relevant(context) is True

    item.cancellation_date = timezone.now()
    item.save()
    assert cell.is_relevant(context) is False

    item.cancellation_date = None
    item.save()

    content = cell.render(context)
    assert '12345' in content
