rm -f coverage.xml
rm -f test_results.xml

export PIP_USE_WHEEL=no
pip install --upgrade pip
pip install --upgrade pylint==1.4.0 astroid==1.3.2 # 1.4.1 is buggy
pip install --upgrade pytest pytest-django pytest-cov webtest
pip install --upgrade 'git+http://repos.entrouvert.org/combo.git/'
pip install --upgrade 'git+http://repos.entrouvert.org/eopayment.git/'
pip install -e .

DJANGO_SETTINGS_MODULE=combo.settings COMBO_SETTINGS_FILE=tests/settings.py py.test --junitxml=test_results.xml --cov-report xml --cov=lingo/ --cov-config .coveragerc tests/

test -f pylint.out && cp pylint.out pylint.out.prev
(pylint -f parseable --rcfile /var/lib/jenkins/pylint.django.rc lingo | tee pylint.out) || /bin/true
test -f pylint.out.prev && (diff pylint.out.prev pylint.out | grep '^[><]' | grep .py) || /bin/true
