# lingo - basket and payment system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import (CreateView, UpdateView, ListView,
        DeleteView, TemplateView)

import eopayment

from .models import Regie, Transaction

class RegieListView(ListView):
    model = Regie


class RegieCreateView(CreateView):
    model = Regie
    success_url = reverse_lazy('lingo-manager-regie-list')


class RegieUpdateView(UpdateView):
    model = Regie
    success_url = reverse_lazy('lingo-manager-regie-list')


class RegieDeleteView(DeleteView):
    model = Regie
    success_url = reverse_lazy('lingo-manager-regie-list')


class TransactionListView(ListView):
    model = Transaction
    paginate_by = 10

    def get_queryset(self):
        return Transaction.objects.filter(status=eopayment.PAID).order_by('-start_date')


class ManagerHomeView(TemplateView):
    template_name = 'lingo/manager_home.html'
