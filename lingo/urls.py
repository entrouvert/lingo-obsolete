# lingo - basket and payment system
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, url, include

from combo.urls_utils import decorated_includes, manager_required

from .views import (RegiesApiView, AddBasketItemApiView, PayView, CallbackView,
                    ReturnView, ItemDownloadView, ItemView)
from .manager_views import (RegieListView, RegieCreateView, RegieUpdateView,
        RegieDeleteView, TransactionListView, ManagerHomeView)

lingo_manager_urls = patterns('lingo.manager_views',
    url('^$', ManagerHomeView.as_view(), name='lingo-manager-homepage'),
    url('^transactions/$', TransactionListView.as_view(), name='lingo-manager-transactions-list'),
    url('^regies/$', RegieListView.as_view(), name='lingo-manager-regie-list'),
    url('^regies/add/$', RegieCreateView.as_view(), name='lingo-manager-regie-add'),
    url('^regies/(?P<pk>\w+)/edit$', RegieUpdateView.as_view(),
        name='lingo-manager-regie-edit'),
    url('^regies/(?P<pk>\w+)/delete$', RegieDeleteView.as_view(),
        name='lingo-manager-regie-delete'),
)

urlpatterns = patterns('',
    url('^api/lingo/regies$', RegiesApiView.as_view(), name='api-regies'),
    url('^api/lingo/add-basket-item$', AddBasketItemApiView.as_view(),
        name='api-add-basket-item'),
    url(r'^lingo/pay$', PayView.as_view(), name='lingo-pay'),
    url(r'^lingo/callback/(?P<regie_pk>\w+)/$', CallbackView.as_view(), name='lingo-callback'),
    url(r'^lingo/return/(?P<regie_pk>\w+)/$', ReturnView.as_view(), name='lingo-return'),
    url(r'^manage/lingo/', decorated_includes(manager_required,
        include(lingo_manager_urls))),
    url(r'^lingo/item/(?P<regie_id>[\w,-]+)/(?P<item_id>[\w,-]+)/pdf$',
        ItemDownloadView.as_view(), name='download-item-pdf'),
    url(r'^lingo/item/(?P<regie_id>[\w,-]+)/(?P<item_id>[\w,-]+)/$',
        ItemView.as_view(), name='view-item'),
)
