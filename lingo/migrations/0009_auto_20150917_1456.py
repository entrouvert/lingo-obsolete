# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('lingo', '0008_auto_20150908_1538'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='regie',
            field=models.ForeignKey(to='lingo.Regie', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='transaction',
            name='remote_items',
            field=models.CharField(default='', max_length=512),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
