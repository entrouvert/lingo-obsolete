# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('lingo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_date', models.DateTimeField(auto_now_add=True)),
                ('end_date', models.DateTimeField(null=True)),
                ('bank_data', jsonfield.fields.JSONField(default=dict, blank=True)),
                ('order_id', models.CharField(max_length=200)),
                ('items', models.ManyToManyField(to='lingo.BasketItem', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
