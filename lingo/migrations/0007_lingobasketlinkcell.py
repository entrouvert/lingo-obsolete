# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        ('data', '0005_auto_20150226_0903'),
        ('lingo', '0006_transaction_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='LingoBasketLinkCell',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Groups', blank=True)),
                ('page', models.ForeignKey(to='data.Page')),
            ],
            options={
                'verbose_name': 'Basket Link',
            },
            bases=(models.Model,),
        ),
    ]
