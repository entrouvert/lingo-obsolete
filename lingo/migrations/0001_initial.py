# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('data', '0005_auto_20150226_0903'),
    ]

    operations = [
        migrations.CreateModel(
            name='BasketItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(max_length=64, verbose_name='Subject')),
                ('source_url', models.URLField(verbose_name='Source URL')),
                ('details', models.TextField(verbose_name='Details', blank=True)),
                ('amount', models.DecimalField(verbose_name='Amount', max_digits=8, decimal_places=2)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('cancellation_date', models.DateTimeField(null=True)),
                ('payment_date', models.DateTimeField(null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LingoBasketCell',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('placeholder', models.CharField(max_length=20)),
                ('order', models.PositiveIntegerField()),
                ('slug', models.SlugField(verbose_name='Slug', blank=True)),
                ('public', models.BooleanField(default=True, verbose_name='Public')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='Groups', blank=True)),
                ('page', models.ForeignKey(to='data.Page')),
            ],
            options={
                'verbose_name': 'Basket',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Regie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=64, verbose_name='Label')),
                ('slug', models.SlugField(unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                ('service', models.CharField(max_length=64, verbose_name='Payment Service', choices=[(b'dummy', 'Dummy (for tests)'), (b'systempayv2', b'systempay (Banque Populaire)'), (b'sips', b'SIPS'), (b'spplus', "SP+ (Caisse d'epargne)")])),
                ('service_options', jsonfield.fields.JSONField(default=dict, verbose_name='Payment Service Options', blank=True)),
            ],
            options={
                'verbose_name': 'Regie',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='basketitem',
            name='regie',
            field=models.ForeignKey(to='lingo.Regie'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basketitem',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
