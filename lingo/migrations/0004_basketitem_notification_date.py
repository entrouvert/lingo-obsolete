# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lingo', '0003_auto_20150306_1047'),
    ]

    operations = [
        migrations.AddField(
            model_name='basketitem',
            name='notification_date',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
